/* Needed gulp config */
var gulp = require('gulp');
var sass = require('gulp-sass');
var scsslint = require('gulp-scss-lint');
var cache = require('gulp-cached');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var neat = require('node-neat');
var notify = require('gulp-notify');
var minifycss = require('gulp-minify-css');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

/* Scripts task */
/*gulp.task('scripts', function() {
  return gulp.src([
    *//* Add your JS files here, they will be combined in this order *//*
    'js/vendor/jquery-1.11.1.js',
    'js/app.js'
    ])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('js'));
});*/

gulp.task('scss-lint', function() {
  return gulp.src('static/sass/*.scss')
    .pipe(cache('scsslint'))
    .pipe(scsslint());
});

/* Sass task */
gulp.task('sass', function () {
    gulp.src('static/sass/style.scss')
    .pipe(plumber())
    .pipe(sass({
        includePaths: ['scss'].concat(neat)
    }))
    .pipe(autoprefixer({
        browsers: ['last 3 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('static/css'))
/*    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('css'))*/
    /* Reload the browser CSS after every change */
    .pipe(reload({stream:true}))
    .pipe(notify({ message: 'Styles task complete' }));
});

/* Reload task */
gulp.task('bs-reload', function () {
    browserSync.reload();
});

/* Prepare Browser-sync for localhost */
gulp.task('browser-sync', function() {
    browserSync.init(['static/css/*.css', 'static/js/*.js'], {
        /*
        I like to use a vhost, WAMP guide: https://www.kristengrote.com/blog/articles/how-to-set-up-virtual-hosts-using-wamp, XAMP guide: http://sawmac.com/xampp/virtualhosts/
        */
        proxy: '127.0.0.1:8000'
        /* For a static server you would use this: */
        /*
        server: {
            baseDir: './'
        }
        */
    });
});

/* Watch scss, js and html files, doing different things with each. */
gulp.task('default', ['scss-lint', 'sass', 'browser-sync'], function () {
    /* Watch scss, run the sass task on change. */
    gulp.watch(['static/sass/*.scss', 'static/sass/**/*.scss'], ['scss-lint', 'sass'])
    /* Watch app.js file, run the scripts task on change. */
    //gulp.watch(['static/js/app.js'], ['scripts'])
    /* Watch .html files, run the bs-reload task on change. */
    gulp.watch(['templates/*.html'], ['bs-reload']);
});
