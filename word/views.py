from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import View

class HomePageView(View):
    def get(self, request):
        # <view logic>
        return render(request, 'index.html', {'form': ''})

